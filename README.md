# EP2 - OO 2019.1 (UnB - Gama)

## Sobre

O projeto simula um software de uma transportadora que pode adicionar veículos a sua frota, remover veículos, adicionar uma nova margem lucro para operar e realizar entregas(fretes).  

O projeto foi criado na IDE IntelliJ IDEA Community.

Para executar o programa abra o IntelliJ e vá em "File"->"Open" e procure o diretório onde o projeto foi extraído.  

Defina a main como a controller localizada em "src/controller.controller.java".  

Pressione (Ctrl + F9) para compilar e (Shift + F10) para executar.  

## Layout e como utilizar  
**Tela inicial**  
![alt text](./images/mainScreen.png "Tela inicial")  

Na tela principal é possível escolher todas as funções do software criando um novo JFrame assim que clicada.

**Tela de margem de Lucro**  
![alt text](./images/margem.png  "Tela de margem de lucro")  

Nesta tela é possível inserir uma margem de lucro para operar ou substituir uma existente. Atenção ao fato de que a margem de lucro é recebida como um valor inteiro em reais.  

**Tela de adição de veículo**  
![alt text](./images/addVehicle.png  "Adição de veículos")  

Nesta tela é possível adicionar novos veículos a frota e também limpar o estoque removendo todos os existentes atualmente. Atenção ao fato de que só serão aceitos veículos cadastrados na formatação pedida, caso contrário o software pedirá para o usuário inserir novamente.  

**Tela de remoção de veículo**  
![alt text](./images/removeVehicle.png "Remoção de veículos")  

Nesta tela é possível remover um veículo por vez da frota.  

**Tela de visualização da frota**  
![alt text](./images/vehicleStock.png "Tabela de visualização de veículos")  

A única função desta tela é possibilitar o usuário a ver a atual frota de veículos.

**Tela de entregas**  
![alt text](./images/shipping.png "Tela de cálculo e resultado de entregas")  

Nesta tela é possível inserir dados para realização de entregas e escolher dentre 3 opções disponíveis para realizar uma viagem e ver seu lucro total para a rodada atual de entregas.      

## Problema

Os dados usados para cálculos foram os seguintes:  
- Carreta
  - **Combustível**: Diesel
  - **Rendimento**: 8 Km/L
  - **Carga máxima**: 30 toneladas
  - **Velocidade média**: 60 Km/h
  - A cada Kg de carga, o rendimento é reduzido em 0.0002 Km/L
- Van
  - **Combustível**: Diesel
  - **Rendimento**: 10 Km/L
  - **Carga máxima**: 3,5 toneladas
  - **Velocidade média**: 80 Km/h
  - A cada Kg de carga, o rendimento é reduzido em 0.001 Km/L
- Carro
  - **Combustível**: Gasolina ou Álcool
  - **Rendimento**: 14 Km/L com gasolina, 12Km/L com álcool
  - **Carga máxima**: 360 Kg
  - **Velocidade média**: 100 Km/h
  - A cada Kg de carga, o rendimento é reduzido em 0.025 Km/L com gasolina e 0.0231 Km/L com álcool
- Moto
  - **Combustível**: Gasolina ou Álcool
  - **Rendimento**: 50 Km/L com gasolina, 43 Km/L com álcool
  - **Carga máxima**: 50 kg
  - **Velocidade média**: 110 Km/h
  - A cada Kg de carga, o rendimento é reduzido em 0.3 Km/L com gasolina e 0.4 Km/L com álcool


  Os valores dos combustíveis a serem utilizados são:
  - **Álcool**: R$ 3.499 por litro
  - **Gasolina**: R$ 4.449 por litro
  - **Diesel**: R$ 3.869 por litro
  
## Problemas conhecidos  

- Ao inserir caracteres que não são números de 0 a 9 nos JTextFields que requerem números, o programa pode agir de forma irregular.  
- É possível escrever na lista na tela de visualização da frota porém se o JFrame for fechado e aberto novamente a tabela volta ao normal pois ela é atualizada toda vez que o botão é pressionado.  
- Se o arquivo estoque.txt não existir o programa não conseguirá funcionar corretamente, porém não foram encontrados erros caso o lucro.txt não exista.  
- Ao tentar realizar uma entrega sem margem de lucro o software não dará resposta ao confirmar os dados então sempre insira uma margem de lucro antes de realizar uma entrega.      

## Observações  

Para sair de qualquer tela apenas clique no botão padrão de sair(o "x" no topo do Frame), exceto no menu principal onde o botão de sair fecha o software.  

Os arquivos estoque.txt e lucro.txt devem sempre existir mesmo vazios para evitar problemas.  

Ao remover um veículo o estoque.txt apaga todas as ocorrências do veículo a ser removido e re-escreve no fim do .txt logo a ordenação muda constantemente mas nada que atrapalhe no funcionamento do software.  

Após algumas tentativas falhas de gerar o .jar optei por não o incluir no repositório pois não estava rodando em outros Sistemas Operacionais.  

Todos os JTextFields recebem as strings apenas após pressionar a tecla "Enter" do teclado.  

Mesmo se não houver um veículo do tipo inserido na frota, a função de remover veículos ainda notificará que um veículo foi removido, porém isto não interfere no funcionamento do código.  

Para novas entregas sempre pressione o botão "Confirmar" após inserção de dados para que o software analise novamente a frota e mostre possibilidades atualizadas.  
## Bibliografia  

-Apostila Java Orientação a Objetos Caelum -- https://www.caelum.com.br/apostila-java-orientacao-objetos/    

-Stack Overflow para inúmeras dúvidas em relação a linguagem -- https://stackoverflow.com/     

-Site para criação do diagrama de classes -- https://www.genmymodel.com/  

## Diagrama de classes  

![alt text](./images/classDiagram.png "Diagrama das classes da model")  