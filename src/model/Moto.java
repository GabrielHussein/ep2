package model;

public class Moto extends Automovel {
    public Moto()
    {
        setNome("Moto");
        setCargaMaxima(50.0f);
        setVelocidadeMedia(110.0f);
        setPossibilidadeEntrega(false);
    }

    public float calculoFrete()
    {
        tempoPrevisto();
        checarPossibilidade();
        if(isPossibilidadeEntrega())
        {
            setCombustivel("Gasolina");
            setRendimento(50.0f);
            setReducaoRendimento(0.3f);
            setCustoCombustivel(4.449f);
            rendimentoReal();
            setFrete((((2*getDistanciaEntrega())/getRendimento())*getCustoCombustivel()));
            auxCombustivel = getFrete();
            setCombustivel("Álcool");
            setRendimento(43.0f);
            setReducaoRendimento(0.4f);
            setCustoCombustivel(3.499f);
            rendimentoReal();
            setFrete((((2*getDistanciaEntrega())/getRendimento())*getCustoCombustivel()));
            auxCombustivel2 = getFrete();
            if(auxCombustivel2<=auxCombustivel)
            {
                return auxCombustivel2;
            }
            else
            {
                return auxCombustivel;
            }
        }
        else {
            return 0;
        }
    }
}
