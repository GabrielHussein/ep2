package model;

public class Carreta extends Automovel {
    public Carreta()
    {
        setNome("Carreta");
        setCombustivel("Diesel");
        setCargaMaxima(30000.0f);
        setVelocidadeMedia(60.0f);
        setRendimento(8.0f);
        setReducaoRendimento(0.0002f);
        setCustoCombustivel(3.869f);
        setPossibilidadeEntrega(false);
    }

    public float calculoFrete()
    {
        tempoPrevisto();
        checarPossibilidade();
        if(isPossibilidadeEntrega())
        {
            rendimentoReal();
            return ((((2*getDistanciaEntrega())/getRendimento())*getCustoCombustivel()));
        }
        else {
            return 0;
        }
    }
}

