package model;

import java.util.ArrayList;

public abstract class Automovel {
    protected String combustivel;
    protected String nome;
    protected float cargaMaxima;
    protected float velocidadeMedia;
    protected float rendimento;
    protected float custoCombustivel;
    protected float margemLucro;
    protected boolean possibilidadeEntrega;
    protected float reducaoRendimento;
    protected float tempoEntrega;
    protected float cargaEntrega;
    protected float distanciaEntrega;
    protected float frete;
    protected float tempoMaximo;
    protected float auxCombustivel;
    protected float auxCombustivel2;
    protected float custoBeneficio;

    public String getCombustivel()
    {
        return combustivel;
    }
    public void setCombustivel(String combustivel)
    {
        this.combustivel = combustivel;
    }

    public float getCargaMaxima()
    {

        return cargaMaxima;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setCargaMaxima(float cargaMaxima)
    {
        this.cargaMaxima = cargaMaxima;
    }

    public float getVelocidadeMedia()
    {
        return velocidadeMedia;
    }
    public void setVelocidadeMedia(float velocidadeMedia)
    {
        this.velocidadeMedia = velocidadeMedia;
    }

    public float getRendimento()
    {
        return rendimento;
    }
    public void setRendimento(float rendimento)
    {
        this.rendimento = rendimento;
    }

    public float getReducaoRendimento()
    {
        return reducaoRendimento;
    }
    public void setReducaoRendimento(float reducaoRendimento)
    {
        this.reducaoRendimento = reducaoRendimento;
    }

    public float getCustoCombustivel()
    {
        return custoCombustivel;
    }
    public void setCustoCombustivel(float custoCombustivel)
    {
        this.custoCombustivel = custoCombustivel;
    }
    public float getMargemLucro() {
        return margemLucro;
    }

    public void setMargemLucro(float margemLucro) {
        this.margemLucro = margemLucro;
    }

    public boolean isPossibilidadeEntrega() {
        return possibilidadeEntrega;
    }

    public void setPossibilidadeEntrega(boolean possibilidadeEntrega) {
        this.possibilidadeEntrega = possibilidadeEntrega;
    }

    public float getTempoEntrega() {
        return tempoEntrega;
    }

    public void setTempoEntrega(float tempoEntrega) {
        this.tempoEntrega = tempoEntrega;
    }

    public float getCargaEntrega() {
        return cargaEntrega;
    }

    public void setCargaEntrega(float cargaEntrega) {
        this.cargaEntrega = cargaEntrega;
    }

    public float getDistanciaEntrega() {
        return distanciaEntrega;
    }

    public void setDistanciaEntrega(float distanciaEntrega) {
        this.distanciaEntrega = distanciaEntrega;
    }

    public float getFrete(){
        return frete;
    }

    public void setFrete(float frete){
        this.frete = frete;
    }

    public float getTempoMaximo() {
        return tempoMaximo;
    }

    public void setTempoMaximo(float tempoMaximo) {
        this.tempoMaximo = tempoMaximo;
    }

    public float getCustoBeneficio() {
        return custoBeneficio;
    }

    public void setCustoBeneficio(float custoBeneficio) {
        this.custoBeneficio = custoBeneficio;
    }

    public void checarPossibilidade()
    {
        if(getCargaEntrega() <= getCargaMaxima() && getTempoEntrega() <= getTempoMaximo())
        {
            setPossibilidadeEntrega(true);
        }
        else
        {
            setPossibilidadeEntrega(false);
        }
    }

    public void tempoPrevisto()
    {
        setTempoEntrega(getDistanciaEntrega()/getVelocidadeMedia());
    }

    public void rendimentoReal()
    {
        setRendimento(getRendimento() - (getCargaEntrega() * getReducaoRendimento()));
    }

    public void custoBeneficio()
    {
        setCustoBeneficio((getFrete())/(getTempoEntrega()));
    }

}
