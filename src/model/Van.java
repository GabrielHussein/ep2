package model;

public class Van extends Automovel {
    public Van()
    {
        setNome("Van");
        setCombustivel("Diesel");
        setCargaMaxima(3500.0f);
        setVelocidadeMedia(80.0f);
        setRendimento(10.0f);
        setReducaoRendimento(0.001f);
        setCustoCombustivel(3.869f);
        setPossibilidadeEntrega(false);
    }

    public float calculoFrete()
    {
        tempoPrevisto();
        checarPossibilidade();
        if(isPossibilidadeEntrega())
        {
            rendimentoReal();
            return ((((2*getDistanciaEntrega())/getRendimento())*getCustoCombustivel()));
        }
        else {
            return 0;
        }
    }
}
