package model;

public class Carro extends Automovel {
    public Carro()
    {
        setNome("Carro");
        setCargaMaxima(360.0f);
        setVelocidadeMedia(100.0f);
        setPossibilidadeEntrega(false);
    }

    public float calculoFrete()
    {
        tempoPrevisto();
        checarPossibilidade();
        if(isPossibilidadeEntrega())
        {
            setCombustivel("Gasolina");
            setRendimento(14.0f);
            setReducaoRendimento(0.025f);
            setCustoCombustivel(4.449f);
            rendimentoReal();
            setFrete((((2*getDistanciaEntrega())/getRendimento())*getCustoCombustivel()));
            auxCombustivel = getFrete();
            setCombustivel("Álcool");
            setRendimento(12.0f);
            setReducaoRendimento(0.0231f);
            setCustoCombustivel(3.499f);
            rendimentoReal();
            setFrete((((2*getDistanciaEntrega())/getRendimento())*getCustoCombustivel()));
            auxCombustivel2 = getFrete();
            if(auxCombustivel2<=auxCombustivel)
            {
                return auxCombustivel2;
            }
            else
            {
                return auxCombustivel;
            }
        }
        else {
            return 0;
        }
    }
}
