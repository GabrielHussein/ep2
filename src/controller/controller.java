package controller;

import model.*;

import view.mainMenu;

import java.io.*;

public class controller {

    public static void main(String[] args) {
        new mainMenu().init();
    }

    public static String doCustoBeneficioCusto(String custoBeneficio, float length, float carga, float time) throws IOException {
        int carro = 0, carreta = 0, van = 0, moto = 0;
        float carroF = 0.0f, vanF = 0.0f, motoF = 0.0f, carretaF = 0.0f, custoCarro = 0.0f, custoVan = 0.0f, custoMoto = 0.0f, custoCarreta = 0.0f;
        BufferedReader in = new BufferedReader(new FileReader("estoque.txt"));
        String type;
        while ((type = in.readLine()) != null) {
            switch (type) {
                case "Carro":
                    carro++;
                    break;
                case "Carreta":
                    carreta++;
                    break;
                case "Van":
                    van++;
                    break;
                case "Moto":
                    moto++;
                    break;
            }
        }
        if (moto > 0) {
            Moto novaMoto = new Moto();
            novaMoto.setDistanciaEntrega(length);
            novaMoto.setCargaEntrega(carga);
            novaMoto.setTempoMaximo(time);
            motoF = novaMoto.calculoFrete();
            novaMoto.setFrete(motoF);
            novaMoto.tempoPrevisto();
            novaMoto.custoBeneficio();
            custoMoto = novaMoto.getCustoBeneficio();
        }
        if (carro > 0) {
            Carro novoCarro = new Carro();
            novoCarro.setDistanciaEntrega(length);
            novoCarro.setCargaEntrega(carga);
            novoCarro.setTempoMaximo(time);
            carroF = novoCarro.calculoFrete();
            novoCarro.setFrete(carroF);
            novoCarro.tempoPrevisto();
            novoCarro.custoBeneficio();
            custoCarro = novoCarro.getCustoBeneficio();
        }
        if (carreta > 0) {
            Carreta novaCarreta = new Carreta();
            novaCarreta.setDistanciaEntrega(length);
            novaCarreta.setCargaEntrega(carga);
            novaCarreta.setTempoMaximo(time);
            carretaF = novaCarreta.calculoFrete();
            novaCarreta.setFrete(carretaF);
            novaCarreta.tempoPrevisto();
            novaCarreta.custoBeneficio();
            custoCarreta = novaCarreta.getCustoBeneficio();
        }
        if (van > 0) {
            Van novaVan = new Van();
            novaVan.setDistanciaEntrega(length);
            novaVan.setCargaEntrega(carga);
            novaVan.setTempoMaximo(time);
            vanF = novaVan.calculoFrete();
            novaVan.setFrete(vanF);
            novaVan.tempoPrevisto();
            novaVan.custoBeneficio();
            custoVan = novaVan.getCustoBeneficio();
        }
        float custoBeneficioF = Float.parseFloat(custoBeneficio);
        if(custoBeneficioF == custoCarro)
        {
            return Float.toString(carroF);
        }
        else if(custoBeneficioF == custoCarreta)
        {
            return Float.toString(carretaF);
        }
        else if(custoBeneficioF == custoMoto)
        {
            return Float.toString(motoF);
        }
        else if(custoBeneficioF == custoVan)
        {
            return Float.toString(vanF);
        }
        return custoBeneficio;
    }

    public static String doShippingCostBenefit(float length, float carga, float time) throws IOException {
        int carro = 0, carreta = 0, van = 0, moto = 0;
        float carroF = 0.0f, vanF = 0.0f, motoF = 0.0f, carretaF = 0.0f, custoBeneficio = 0.0f;
        BufferedReader in = new BufferedReader(new FileReader("estoque.txt"));
        String type;
        while ((type = in.readLine()) != null) {
            switch (type) {
                case "Carro":
                    carro++;
                    break;
                case "Carreta":
                    carreta++;
                    break;
                case "Van":
                    van++;
                    break;
                case "Moto":
                    moto++;
                    break;
            }
        }
        if (moto > 0) {
            Moto novaMoto = new Moto();
            novaMoto.setDistanciaEntrega(length);
            novaMoto.setCargaEntrega(carga);
            novaMoto.setTempoMaximo(time);
            motoF = novaMoto.calculoFrete();
            novaMoto.setFrete(motoF);
            novaMoto.tempoPrevisto();
            novaMoto.custoBeneficio();
            motoF = novaMoto.getCustoBeneficio();
        }
        if (carro > 0) {
            Carro novoCarro = new Carro();
            novoCarro.setDistanciaEntrega(length);
            novoCarro.setCargaEntrega(carga);
            novoCarro.setTempoMaximo(time);
            carroF = novoCarro.calculoFrete();
            novoCarro.setFrete(carroF);
            novoCarro.tempoPrevisto();
            novoCarro.custoBeneficio();
            carroF = novoCarro.getCustoBeneficio();
        }
        if (carreta > 0) {
            Carreta novaCarreta = new Carreta();
            novaCarreta.setDistanciaEntrega(length);
            novaCarreta.setCargaEntrega(carga);
            novaCarreta.setTempoMaximo(time);
            carretaF = novaCarreta.calculoFrete();
            novaCarreta.setFrete(carretaF);
            novaCarreta.tempoPrevisto();
            novaCarreta.custoBeneficio();
            carretaF = novaCarreta.getCustoBeneficio();
        }
        if (van > 0) {
            Van novaVan = new Van();
            novaVan.setDistanciaEntrega(length);
            novaVan.setCargaEntrega(carga);
            novaVan.setTempoMaximo(time);
            vanF = novaVan.calculoFrete();
            novaVan.setFrete(vanF);
            novaVan.tempoPrevisto();
            novaVan.custoBeneficio();
            vanF = novaVan.getCustoBeneficio();
        }
        if(custoBeneficio<carroF)
        {
            custoBeneficio = carroF;
        }
        if(custoBeneficio<carretaF)
        {
            custoBeneficio = carretaF;
        }
        if(custoBeneficio<motoF)
        {
            custoBeneficio = motoF;
        }
        if(custoBeneficio<vanF)
        {
            custoBeneficio = vanF;
        }
        return Float.toString(custoBeneficio);
    }

    public static String doMenorTempoCusto(String menorTempo, float length, float carga, float time) throws IOException {
        int carro = 0, carreta = 0, van = 0, moto = 0;
        float carroF = 0.0f, vanF = 0.0f, motoF = 0.0f, carretaF = 0.0f, timeCarro = 0.0f, timeVan = 0.0f, timeCarreta = 0.0f, timeMoto = 0.0f;
        BufferedReader in = new BufferedReader(new FileReader("estoque.txt"));
        String type;
        while ((type = in.readLine()) != null) {
            switch (type) {
                case "Carro":
                    carro++;
                    break;
                case "Carreta":
                    carreta++;
                    break;
                case "Van":
                    van++;
                    break;
                case "Moto":
                    moto++;
                    break;
            }
        }
        if (moto > 0) {
            Moto novaMoto = new Moto();
            novaMoto.setDistanciaEntrega(length);
            novaMoto.setCargaEntrega(carga);
            novaMoto.setTempoMaximo(time);
            motoF = novaMoto.calculoFrete();
            novaMoto.tempoPrevisto();
            timeMoto = novaMoto.getTempoEntrega();
            novaMoto.setFrete(motoF);
        }
        if (carro > 0) {
            Carro novoCarro = new Carro();
            novoCarro.setDistanciaEntrega(length);
            novoCarro.setCargaEntrega(carga);
            novoCarro.setTempoMaximo(time);
            carroF = novoCarro.calculoFrete();
            novoCarro.tempoPrevisto();
            timeCarro = novoCarro.getTempoEntrega();
            novoCarro.setFrete(carroF);
        }
        if (carreta > 0) {
            Carreta novaCarreta = new Carreta();
            novaCarreta.setDistanciaEntrega(length);
            novaCarreta.setCargaEntrega(carga);
            novaCarreta.setTempoMaximo(time);
            carretaF = novaCarreta.calculoFrete();
            novaCarreta.tempoPrevisto();
            timeCarreta = novaCarreta.getTempoEntrega();
            novaCarreta.setFrete(carretaF);
        }
        if (van > 0) {
            Van novaVan = new Van();
            novaVan.setDistanciaEntrega(length);
            novaVan.setCargaEntrega(carga);
            novaVan.setTempoMaximo(time);
            vanF = novaVan.calculoFrete();
            novaVan.tempoPrevisto();
            timeVan = novaVan.getTempoEntrega();
            novaVan.setFrete(vanF);
        }
        float menorTempoF = Float.parseFloat(menorTempo);
        if(menorTempoF == timeCarro)
        {
            return Float.toString(carroF);
        }
        else if(menorTempoF == timeCarreta)
        {
            return Float.toString(carretaF);
        }
        else if(menorTempoF == timeMoto)
        {
            return Float.toString(motoF);
        }
        else if(menorTempoF == timeVan)
        {
            return Float.toString(vanF);
        }
        return Float.toString(menorTempoF);
    }

    public static String doCustoBeneficioTime(float custoBeneficio, float length, float carga, float time) throws IOException {
        int carro = 0, carreta = 0, van = 0, moto = 0;
        float carroF = 0.0f, carretaF = 0.0f, vanF = 0.0f, motoF = 0.0f;
        float timeCarro = 0.0f, timeVan = 0.0f, timeMoto = 0.0f, timeCarreta = 0.0f, timeFinal = 0.0f;
        BufferedReader in = new BufferedReader(new FileReader("estoque.txt"));
        String type;
        while ((type = in.readLine()) != null) {
            switch (type) {
                case "Carro":
                    carro++;
                    break;
                case "Carreta":
                    carreta++;
                    break;
                case "Van":
                    van++;
                    break;
                case "Moto":
                    moto++;
                    break;
            }
        }
        if (moto > 0) {
            Moto novaMoto = new Moto();
            novaMoto.setDistanciaEntrega(length);
            novaMoto.setCargaEntrega(carga);
            novaMoto.setTempoMaximo(time);
            motoF = novaMoto.calculoFrete();
            timeMoto = novaMoto.getTempoEntrega();
            novaMoto.setFrete(motoF);
        }
        if (carro > 0) {
            Carro novoCarro = new Carro();
            novoCarro.setDistanciaEntrega(length);
            novoCarro.setCargaEntrega(carga);
            novoCarro.setTempoMaximo(time);
            carroF = novoCarro.calculoFrete();
            timeCarro = novoCarro.getTempoEntrega();
            novoCarro.setFrete(carroF);
        }
        if (carreta > 0) {
            Carreta novaCarreta = new Carreta();
            novaCarreta.setDistanciaEntrega(length);
            novaCarreta.setCargaEntrega(carga);
            novaCarreta.setTempoMaximo(time);
            carretaF = novaCarreta.calculoFrete();
            timeCarreta = novaCarreta.getTempoEntrega();
            novaCarreta.setFrete(carretaF);
        }
        if (van > 0) {
            Van novaVan = new Van();
            novaVan.setDistanciaEntrega(length);
            novaVan.setCargaEntrega(carga);
            novaVan.setTempoMaximo(time);
            vanF = novaVan.calculoFrete();
            timeVan = novaVan.getTempoEntrega();
            novaVan.setFrete(vanF);
        }
        if (custoBeneficio == carroF) {
            timeFinal = timeCarro;
        }
        if (custoBeneficio == carretaF) {
            timeFinal = timeCarreta;
        }
        if (custoBeneficio == motoF) {
            timeFinal = timeMoto;
        }
        if (custoBeneficio == vanF)
        {
            timeFinal = timeVan;
        }
        return Float.toString(timeFinal);
    }

    public static String doShippingTime(float length, float carga, float time) throws IOException {
        int carro = 0, carreta = 0, van = 0, moto = 0;
        float timeCarro = 0.0f, timeVan = 0.0f, timeMoto = 0.0f, timeCarreta = 0.0f, timeFinal = 10000000.0f;
        BufferedReader in = new BufferedReader(new FileReader("estoque.txt"));
        String type;
        while ((type = in.readLine()) != null) {
            switch (type) {
                case "Carro":
                    carro++;
                    break;
                case "Carreta":
                    carreta++;
                    break;
                case "Van":
                    van++;
                    break;
                case "Moto":
                    moto++;
                    break;
            }
        }
        if (moto > 0) {
            Moto novaMoto = new Moto();
            novaMoto.setDistanciaEntrega(length);
            novaMoto.setCargaEntrega(carga);
            novaMoto.setTempoMaximo(time);
            novaMoto.tempoPrevisto();
            timeMoto = novaMoto.getTempoEntrega();
            novaMoto.checarPossibilidade();
            if(!novaMoto.isPossibilidadeEntrega())
            {
                timeMoto = 0.0f;
            }
        }
        if (carro > 0) {
            Carro novoCarro = new Carro();
            novoCarro.setDistanciaEntrega(length);
            novoCarro.setCargaEntrega(carga);
            novoCarro.setTempoMaximo(time);
            novoCarro.tempoPrevisto();
            timeCarro = novoCarro.getTempoEntrega();
            novoCarro.checarPossibilidade();
            if(!novoCarro.isPossibilidadeEntrega())
            {
                timeCarro = 0.0f;
            }
        }
        if (carreta > 0) {
            Carreta novaCarreta = new Carreta();
            novaCarreta.setDistanciaEntrega(length);
            novaCarreta.setCargaEntrega(carga);
            novaCarreta.setTempoMaximo(time);
            novaCarreta.tempoPrevisto();
            timeCarreta = novaCarreta.getTempoEntrega();
            novaCarreta.checarPossibilidade();
            if(!novaCarreta.isPossibilidadeEntrega())
            {
                timeCarreta = 0.0f;
            }
        }
        if (van > 0) {
            Van novaVan = new Van();
            novaVan.setDistanciaEntrega(length);
            novaVan.setCargaEntrega(carga);
            novaVan.setTempoMaximo(time);
            novaVan.tempoPrevisto();
            timeVan = novaVan.getTempoEntrega();
            novaVan.checarPossibilidade();
            if(!novaVan.isPossibilidadeEntrega())
            {
                timeVan = 0.0f;
            }
        }
        if(timeFinal>timeCarreta && timeCarreta!=0)
        {
            timeFinal = timeCarreta;
        }
        if(timeFinal>timeCarro && timeCarro!=0)
        {
            timeFinal = timeCarro;
        }
        if(timeFinal>timeMoto && timeMoto!=0)
        {
            timeFinal = timeMoto;
        }
        if(timeFinal>timeVan && timeVan!=0)
        {
            timeFinal = timeVan;
        }
        return Float.toString(timeFinal);
    }

    public static String doShippingName(float menorCusto, float length, float carga, float time) throws IOException {
        int carro = 0, carreta = 0, van = 0, moto = 0;
        float carroF = 5000.0f, carretaF = 5000.0f, vanF = 5000.0f, motoF = 5000.0f;
        String nome = null;
        BufferedReader in = new BufferedReader(new FileReader("estoque.txt"));
        String type;
        while ((type = in.readLine()) != null) {
            switch (type) {
                case "Carro":
                    carro++;
                    break;
                case "Carreta":
                    carreta++;
                    break;
                case "Van":
                    van++;
                    break;
                case "Moto":
                    moto++;
                    break;
            }
        }
        if (moto > 0) {
            Moto novaMoto = new Moto();
            novaMoto.setDistanciaEntrega(length);
            novaMoto.setCargaEntrega(carga);
            novaMoto.setTempoMaximo(time);
            motoF = novaMoto.calculoFrete();
            novaMoto.setFrete(motoF);
        }
        if (carro > 0) {
            Carro novoCarro = new Carro();
            novoCarro.setDistanciaEntrega(length);
            novoCarro.setCargaEntrega(carga);
            novoCarro.setTempoMaximo(time);
            carroF = novoCarro.calculoFrete();
            novoCarro.setFrete(carroF);
        }
        if (carreta > 0) {
            Carreta novaCarreta = new Carreta();
            novaCarreta.setDistanciaEntrega(length);
            novaCarreta.setCargaEntrega(carga);
            novaCarreta.setTempoMaximo(time);
            carretaF = novaCarreta.calculoFrete();
            novaCarreta.setFrete(carretaF);
        }
        if (van > 0) {
            Van novaVan = new Van();
            novaVan.setDistanciaEntrega(length);
            novaVan.setCargaEntrega(carga);
            novaVan.setTempoMaximo(time);
            vanF = novaVan.calculoFrete();
            novaVan.setFrete(vanF);
        }
        if (menorCusto == vanF)
        {
            nome = "Van";
        }
        if (menorCusto == carroF) {
            nome = "Carro";
        }
        if (menorCusto == carretaF) {
            nome = "Carreta";
        }
        if (menorCusto == motoF) {
            nome = "Moto";
        }
        return nome;
    }

    public static String doMenorCustoTime(float menorCusto, float length, float carga, float time) throws IOException {
        int carro = 0, carreta = 0, van = 0, moto = 0;
        float carroF = 0.0f, carretaF = 0.0f, vanF = 0.0f, motoF = 0.0f;
        float timeCarro = 0.0f, timeVan = 0.0f, timeMoto = 0.0f, timeCarreta = 0.0f, timeFinal = 0.0f;
        BufferedReader in = new BufferedReader(new FileReader("estoque.txt"));
        String type;
        while ((type = in.readLine()) != null) {
            switch (type) {
                case "Carro":
                    carro++;
                    break;
                case "Carreta":
                    carreta++;
                    break;
                case "Van":
                    van++;
                    break;
                case "Moto":
                    moto++;
                    break;
            }
        }
        if (moto > 0) {
            Moto novaMoto = new Moto();
            novaMoto.setDistanciaEntrega(length);
            novaMoto.setCargaEntrega(carga);
            novaMoto.setTempoMaximo(time);
            motoF = novaMoto.calculoFrete();
            timeMoto = novaMoto.getTempoEntrega();
            novaMoto.setFrete(motoF);
        }
        if (carro > 0) {
            Carro novoCarro = new Carro();
            novoCarro.setDistanciaEntrega(length);
            novoCarro.setCargaEntrega(carga);
            novoCarro.setTempoMaximo(time);
            carroF = novoCarro.calculoFrete();
            timeCarro = novoCarro.getTempoEntrega();
            novoCarro.setFrete(carroF);
        }
        if (carreta > 0) {
            Carreta novaCarreta = new Carreta();
            novaCarreta.setDistanciaEntrega(length);
            novaCarreta.setCargaEntrega(carga);
            novaCarreta.setTempoMaximo(time);
            carretaF = novaCarreta.calculoFrete();
            timeCarreta = novaCarreta.getTempoEntrega();
            novaCarreta.setFrete(carretaF);
        }
        if (van > 0) {
            Van novaVan = new Van();
            novaVan.setDistanciaEntrega(length);
            novaVan.setCargaEntrega(carga);
            novaVan.setTempoMaximo(time);
            vanF = novaVan.calculoFrete();
            timeVan = novaVan.getTempoEntrega();
            novaVan.setFrete(vanF);
        }
        if (menorCusto == carroF) {
            timeFinal = timeCarro;
        }
        if (menorCusto == carretaF) {
            timeFinal = timeCarreta;
        }
        if (menorCusto == motoF) {
            timeFinal = timeMoto;
        }
        if (menorCusto == vanF)
        {
            timeFinal = timeVan;
        }
        return Float.toString(timeFinal);
    }

    public static String doShipping(float length, float carga, float time) throws IOException {
        int carro = 0, carreta = 0, van = 0, moto = 0;
        float carroF = 0.0f, carretaF = 0.0f, vanF = 0.0f, motoF = 0.0f, menorCusto = 10000000.0f;
        BufferedReader in = new BufferedReader(new FileReader("estoque.txt"));
        String type;
        while ((type = in.readLine()) != null) {
            switch (type) {
                case "Carro":
                    carro++;
                    break;
                case "Carreta":
                    carreta++;
                    break;
                case "Van":
                    van++;
                    break;
                case "Moto":
                    moto++;
                    break;
            }
        }
        if (moto > 0) {
            Moto novaMoto = new Moto();
            novaMoto.setDistanciaEntrega(length);
            novaMoto.setCargaEntrega(carga);
            novaMoto.setTempoMaximo(time);
            motoF = novaMoto.calculoFrete();
            novaMoto.setFrete(motoF);
        }
        if (carro > 0) {
            Carro novoCarro = new Carro();
            novoCarro.setDistanciaEntrega(length);
            novoCarro.setCargaEntrega(carga);
            novoCarro.setTempoMaximo(time);
            carroF = novoCarro.calculoFrete();
            novoCarro.setFrete(carroF);
        }
        if (carreta > 0) {
            Carreta novaCarreta = new Carreta();
            novaCarreta.setDistanciaEntrega(length);
            novaCarreta.setCargaEntrega(carga);
            novaCarreta.setTempoMaximo(time);
            carretaF = novaCarreta.calculoFrete();
            novaCarreta.setFrete(carretaF);
        }
        if (van > 0) {
            Van novaVan = new Van();
            novaVan.setDistanciaEntrega(length);
            novaVan.setCargaEntrega(carga);
            novaVan.setTempoMaximo(time);
            vanF = novaVan.calculoFrete();
            novaVan.setFrete(vanF);
        }
        if (menorCusto > carroF && carroF!=0.0f) {
            menorCusto = carroF;
        }
        if (menorCusto > carretaF && carretaF!=0.0f) {
            menorCusto = carretaF;
        }
        if (menorCusto > motoF && motoF!=0.0f) {
            menorCusto = motoF;
        }
        if (menorCusto > vanF && vanF!=0.0f)
        {
            menorCusto = vanF;
        }
        if (menorCusto == 10000000.0f || menorCusto == 0.0f) {
            return "vazio";
        }
        return Float.toString(menorCusto);
    }

    public static String doMargemLucro(float menorCusto) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader("lucro.txt"));
        StringBuilder stringBuilder = new StringBuilder();
        String l;
        String ls = System.getProperty("line.separator");
        while ((l = reader.readLine()) != null) {
            stringBuilder.append(l);
            stringBuilder.append(ls);
        }
        stringBuilder.deleteCharAt(stringBuilder.length() - 1);
        reader.close();
        String lucro = stringBuilder.toString();
        float lucro1 = Float.parseFloat(lucro) + menorCusto;
        lucro = Float.toString(lucro1);
        return lucro;
    }


    public static void clearEstoque() {
        try {
            FileWriter writer = new FileWriter("estoque.txt", false);
            writer.write("");
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void removeVehicle(String name) throws IOException {
        File currentFile = new File("estoque.txt");
        File tempFile = new File("tempEstoque.txt");
        int counter = -1;
        BufferedReader reader = new BufferedReader(new FileReader(currentFile));
        BufferedWriter writer = new BufferedWriter(new FileWriter(tempFile));

        String vehicleToRemove = name;
        String currentLine;

        while(((currentLine = reader.readLine()) != null)) {
            String trimmedLine = currentLine.trim();
            if(trimmedLine.equals(vehicleToRemove))
            {
                counter++;
                continue;
            }
            writer.write(currentLine + System.getProperty("line.separator"));
        }
        writer.close();
        reader.close();
        boolean successful = tempFile.renameTo(currentFile);
        FileWriter writer2 = new FileWriter("estoque.txt", true);
        if (counter == -1)
        {
            counter = 0;
        }
        while(counter!=0) {
            writer2.write(name);
            writer2.write("\n");
            counter--;
        }
        writer2.close();
    }

    public static boolean saveCarreta(String nome) {
        try {
            FileWriter writer = new FileWriter("estoque.txt", true);
            writer.write("\n");
            writer.write(nome);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return true;
    }

    public static boolean saveVan(String nome) {
        try {
            FileWriter writer = new FileWriter("estoque.txt", true);
            writer.write("\n");
            writer.write(nome);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return true;
    }

    public static boolean saveCarro(String nome) {
        try {
            FileWriter writer = new FileWriter("estoque.txt", true);
            writer.write("\n");
            writer.write(nome);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return true;
    }

    public static boolean saveMoto(String nome) {
        try {
            FileWriter writer = new FileWriter("estoque.txt", true);
            writer.write("\n");
            writer.write(nome);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return true;
    }

    public static void mudarMargemLucro(float margem) {
        try {
            FileWriter writer = new FileWriter("lucro.txt", false);
            String margem1 = Float.toString(margem);
            writer.write(margem1);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String quantCarros() throws IOException {
        int carro = 0;
        BufferedReader in = new BufferedReader(new FileReader("estoque.txt"));
        String type;
        while ((type = in.readLine()) != null) {
            if (type.equals("Carro")) {
                carro++;
            }
        }
        return Integer.toString(carro);
    }
    public static String quantVans() throws IOException {
        int van = 0;
        BufferedReader in = new BufferedReader(new FileReader("estoque.txt"));
        String type;
        while ((type = in.readLine()) != null) {
            if (type.equals("Van")) {
                van++;
            }
        }
        return Integer.toString(van);
    }
    public static String quantCarretas() throws IOException {
        int carreta = 0;
        BufferedReader in = new BufferedReader(new FileReader("estoque.txt"));
        String type;
        while ((type = in.readLine()) != null) {
            if (type.equals("Carreta")) {
                carreta++;
            }
        }
        return Integer.toString(carreta);
    }
    public static String quantMotos() throws IOException {
        int moto = 0;
        BufferedReader in = new BufferedReader(new FileReader("estoque.txt"));
        String type;
        while ((type = in.readLine()) != null) {
            if (type.equals("Moto")) {
                moto++;
            }
        }
        return Integer.toString(moto);
    }
}
