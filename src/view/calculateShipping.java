package view;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import controller.controller;

public class calculateShipping {
    private float timeFloat = 0.0f, cargaFloat = 0.0f, lengthFloat = 0.0f, lucroTotal;
    private JTextField getTime;
    private JTextField getCarga;
    private JTextField getLength;
    private JButton confirmShipping;
    private JLabel maxTime;
    private JLabel carga;
    private JLabel length;
    private JPanel calculateShippingScreen;
    private JButton confirmarButton1;
    private JButton confirmarButton2;
    private JButton confirmarButton3 = new JButton();
    private JPanel confirmScreen;
    private JLabel nameLabel1;
    private JLabel custoLabel1;
    private JLabel timeLabel1;
    private JLabel nameLabel2;
    private JLabel custoLabel2;
    private JLabel timeLabel2;
    private JLabel nameLabel3;
    private JLabel custoLabel3;
    private JLabel timeLabel3;
    private JLabel totalLucro;

    public calculateShipping() {
        getTime.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                String timeString = getTime.getText();
                timeFloat = Float.parseFloat(timeString);
            }
        });
        getCarga.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                String cargaString = getCarga.getText();
                cargaFloat = Float.parseFloat(cargaString);
            }
        });
        getLength.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                String lengthString = getLength.getText();
                lengthFloat = Float.parseFloat(lengthString);
            }
        });
        confirmShipping.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (lengthFloat != 0.0f && cargaFloat != 0.0f && timeFloat != 0.0f) {
                    try {
                        String menorCusto = controller.doShipping(lengthFloat, cargaFloat, timeFloat);
                        if (menorCusto.equals("vazio")) {
                            JOptionPane.showMessageDialog(null, "Não há veículo disponível que consiga realizar a viagem.");
                        } else {
                            float menorCustoF = Float.parseFloat(menorCusto);
                            String menorCustoNome = controller.doShippingName(menorCustoF, lengthFloat, cargaFloat, timeFloat);
                            String menorCustoMargem = controller.doMargemLucro(menorCustoF);
                            String menorCustoTempo = controller.doMenorCustoTime(menorCustoF, lengthFloat, cargaFloat, timeFloat);
                            String nome = "Veículo: " + menorCustoNome + " Custo: " + menorCusto;
                            String tempo = "Tempo estimado de entrega: " + menorCustoTempo + " horas.";
                            String custoFinal = "Custo final: " + menorCustoMargem;
                            nameLabel1.setText(nome);
                            timeLabel1.setText(tempo);
                            custoLabel1.setText(custoFinal);
                            String menorTempo = controller.doShippingTime(lengthFloat, cargaFloat, timeFloat);
                            String menorTempoCusto = controller.doMenorTempoCusto(menorTempo, lengthFloat, cargaFloat, timeFloat);
                            float menorTempoCustoF = Float.parseFloat(menorTempoCusto);
                            String menorTempoNome = controller.doShippingName(menorTempoCustoF, lengthFloat, cargaFloat, timeFloat);
                            String menorTempoMargem = controller.doMargemLucro(menorTempoCustoF);
                            nome = "Veículo: " + menorTempoNome + " Custo: " + menorTempoCusto;
                            tempo = "Tempo estimado de entrega: " + menorTempo + " horas.";
                            custoFinal = "Custo final: " + menorTempoMargem;
                            nameLabel2.setText(nome);
                            timeLabel2.setText(tempo);
                            custoLabel2.setText(custoFinal);
                            String custoBeneficio = controller.doShippingCostBenefit(lengthFloat, cargaFloat, timeFloat);
                            String custoBeneficioCusto = controller.doCustoBeneficioCusto(custoBeneficio, lengthFloat, cargaFloat, timeFloat);
                            float custoBeneficioF = Float.parseFloat(custoBeneficioCusto);
                            String custoBeneficioNome = controller.doShippingName(custoBeneficioF, lengthFloat, cargaFloat, timeFloat);
                            String custoBeneficioMargem = controller.doMargemLucro(custoBeneficioF);
                            String custoBeneficioTempo = controller.doCustoBeneficioTime(custoBeneficioF, lengthFloat, cargaFloat, timeFloat);
                            nome = "Veiculo: " + custoBeneficioNome + " Custo: " + custoBeneficioCusto;
                            tempo = "Tempo estimado de entrega: " + custoBeneficioTempo + " horas.";
                            custoFinal = "Custo final: " + custoBeneficioMargem;
                            nameLabel3.setText(nome);
                            timeLabel3.setText(tempo);
                            custoLabel3.setText(custoFinal);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Um ou mais dos requisitos está faltando, tente novamente(Aperte enter para validar as entradas).");
                }
            }
        });
        confirmarButton1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (confirmarButton1.isEnabled()) {
                    String menorCusto;
                    try {
                        menorCusto = controller.doShipping(lengthFloat, cargaFloat, timeFloat);
                        float menorCustoF = Float.parseFloat(menorCusto);
                        String menorCustoMargem = controller.doMargemLucro(menorCustoF);
                        float menorCustoMargemF = Float.parseFloat(menorCustoMargem);
                        lucroTotal = lucroTotal + menorCustoMargemF - menorCustoF;
                        String lucroTotalS = Float.toString(lucroTotal);
                        lucroTotalS = "Lucro total: " + lucroTotalS;
                        totalLucro.setText(lucroTotalS);
                        String menorCustoNome = controller.doShippingName(menorCustoF, lengthFloat, cargaFloat, timeFloat);
                        controller.removeVehicle(menorCustoNome);
                        JOptionPane.showMessageDialog(null, "Veículo enviado para realizar a entrega, ele foi removido de sua frota.");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        confirmarButton2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (confirmarButton2.isEnabled()) {
                    String menorTempo;
                    try {
                        menorTempo = controller.doShippingTime(lengthFloat, cargaFloat, timeFloat);
                        String menorTempoCusto = controller.doMenorTempoCusto(menorTempo, lengthFloat, cargaFloat, timeFloat);
                        float menorTempoCustoF = Float.parseFloat(menorTempoCusto);
                        String menorTempoMargem = controller.doMargemLucro(menorTempoCustoF);
                        float menorCustoMargemF = Float.parseFloat(menorTempoMargem);
                        lucroTotal = lucroTotal + menorCustoMargemF - menorTempoCustoF;
                        String lucroTotalS = Float.toString(lucroTotal);
                        lucroTotalS = "Lucro total: " + lucroTotalS;
                        totalLucro.setText(lucroTotalS);
                        String menorTempoNome = controller.doShippingName(menorTempoCustoF, lengthFloat, cargaFloat, timeFloat);
                        controller.removeVehicle(menorTempoNome);
                        JOptionPane.showMessageDialog(null, "Veículo enviado para realizar a entrega, ele foi removido de sua frota.");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        confirmarButton3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (confirmarButton3.isEnabled()) {
                    String custoBeneficio;
                    try {
                        custoBeneficio = controller.doShippingCostBenefit(lengthFloat, cargaFloat, timeFloat);
                        String custoBeneficioCusto = controller.doCustoBeneficioCusto(custoBeneficio, lengthFloat, cargaFloat, timeFloat);
                        float custoBeneficioF = Float.parseFloat(custoBeneficioCusto);
                        String custoBeneficioMargem = controller.doMargemLucro(custoBeneficioF);
                        float custoBeneficioMargemF = Float.parseFloat(custoBeneficioMargem);
                        lucroTotal = lucroTotal + custoBeneficioMargemF - custoBeneficioF;
                        String lucroTotalS = Float.toString(lucroTotal);
                        lucroTotalS = "Lucro total: " + lucroTotalS;
                        totalLucro.setText(lucroTotalS);
                        String custoBeneficioNome = controller.doShippingName(custoBeneficioF, lengthFloat, cargaFloat, timeFloat);
                        controller.removeVehicle(custoBeneficioNome);
                        JOptionPane.showMessageDialog(null, "Veículo enviado para realizar a entrega, ele foi removido de sua frota.");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    public static void init() {
        JFrame frame = new JFrame("Realizar entrega");
        frame.setContentPane(new calculateShipping().calculateShippingScreen);
        frame.setLocationRelativeTo(null);
        frame.setResizable(false);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
        frame.setVisible(true);
    }

    {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
        $$$setupUI$$$();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        calculateShippingScreen = new JPanel();
        calculateShippingScreen.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(7, 2, new Insets(0, 0, 0, 0), -1, -1));
        maxTime = new JLabel();
        maxTime.setText("Tempo máximo da entrega(em horas):");
        calculateShippingScreen.add(maxTime, new com.intellij.uiDesigner.core.GridConstraints(1, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        carga = new JLabel();
        carga.setText("Carga a ser entregue(em Kg):");
        calculateShippingScreen.add(carga, new com.intellij.uiDesigner.core.GridConstraints(2, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        getTime = new JTextField();
        calculateShippingScreen.add(getTime, new com.intellij.uiDesigner.core.GridConstraints(1, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        getCarga = new JTextField();
        calculateShippingScreen.add(getCarga, new com.intellij.uiDesigner.core.GridConstraints(2, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        getLength = new JTextField();
        calculateShippingScreen.add(getLength, new com.intellij.uiDesigner.core.GridConstraints(3, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_WANT_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        length = new JLabel();
        length.setText("Distância do destino(em Km):");
        calculateShippingScreen.add(length, new com.intellij.uiDesigner.core.GridConstraints(3, 0, 2, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        confirmShipping = new JButton();
        confirmShipping.setText("Confirmar");
        calculateShippingScreen.add(confirmShipping, new com.intellij.uiDesigner.core.GridConstraints(5, 0, 1, 2, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        confirmScreen = new JPanel();
        confirmScreen.setLayout(new com.intellij.uiDesigner.core.GridLayoutManager(6, 3, new Insets(0, 0, 0, 0), -1, -1));
        confirmScreen.setVisible(true);
        calculateShippingScreen.add(confirmScreen, new com.intellij.uiDesigner.core.GridConstraints(6, 0, 1, 2, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_BOTH, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        confirmarButton1 = new JButton();
        confirmarButton1.setText("Confirmar opção de menor custo");
        confirmScreen.add(confirmarButton1, new com.intellij.uiDesigner.core.GridConstraints(5, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        confirmarButton3 = new JButton();
        confirmarButton3.setLabel("");
        confirmarButton3.setText("Confirmar melhor custo-benefício");
        confirmScreen.add(confirmarButton3, new com.intellij.uiDesigner.core.GridConstraints(5, 2, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        confirmarButton2 = new JButton();
        confirmarButton2.setText("Confirmar opção mais rápida");
        confirmScreen.add(confirmarButton2, new com.intellij.uiDesigner.core.GridConstraints(5, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_CENTER, com.intellij.uiDesigner.core.GridConstraints.FILL_HORIZONTAL, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_SHRINK | com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_CAN_GROW, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        timeLabel3 = new JLabel();
        timeLabel3.setText("");
        confirmScreen.add(timeLabel3, new com.intellij.uiDesigner.core.GridConstraints(3, 2, 2, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        nameLabel1 = new JLabel();
        nameLabel1.setText("");
        confirmScreen.add(nameLabel1, new com.intellij.uiDesigner.core.GridConstraints(1, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        custoLabel1 = new JLabel();
        custoLabel1.setText("");
        confirmScreen.add(custoLabel1, new com.intellij.uiDesigner.core.GridConstraints(2, 0, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        nameLabel2 = new JLabel();
        nameLabel2.setText("");
        confirmScreen.add(nameLabel2, new com.intellij.uiDesigner.core.GridConstraints(1, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        custoLabel2 = new JLabel();
        custoLabel2.setText("");
        confirmScreen.add(custoLabel2, new com.intellij.uiDesigner.core.GridConstraints(2, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        timeLabel2 = new JLabel();
        timeLabel2.setText("");
        confirmScreen.add(timeLabel2, new com.intellij.uiDesigner.core.GridConstraints(3, 1, 2, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        timeLabel1 = new JLabel();
        timeLabel1.setText("");
        confirmScreen.add(timeLabel1, new com.intellij.uiDesigner.core.GridConstraints(3, 0, 2, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        nameLabel3 = new JLabel();
        nameLabel3.setText("");
        confirmScreen.add(nameLabel3, new com.intellij.uiDesigner.core.GridConstraints(1, 2, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        custoLabel3 = new JLabel();
        custoLabel3.setText("");
        confirmScreen.add(custoLabel3, new com.intellij.uiDesigner.core.GridConstraints(2, 2, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        totalLucro = new JLabel();
        totalLucro.setText("");
        confirmScreen.add(totalLucro, new com.intellij.uiDesigner.core.GridConstraints(0, 1, 1, 1, com.intellij.uiDesigner.core.GridConstraints.ANCHOR_WEST, com.intellij.uiDesigner.core.GridConstraints.FILL_NONE, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, com.intellij.uiDesigner.core.GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return calculateShippingScreen;
    }

}
